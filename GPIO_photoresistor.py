import RPi.GPIO as GPIO
import time

def blink(pin, duration):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(duration)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(duration)
    return


GPIO.setmode(GPIO.BOARD)

GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(18, GPIO.IN)

duration = 0.5
loop_pin = [11, 13, 15]

for i in range(0, 10):
    for j in loop_pin:
        GPIO.output(16, GPIO.HIGH)
        day = GPIO.input(18)
        if day == 0:
            blink(j, duration)
        else:
            time.sleep(duration)
    
GPIO.cleanup()
