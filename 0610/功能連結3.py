import tkinter as tk
# 引用必要套件
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import datetime 

# 引用私密金鑰
# path/to/serviceAccount.json 請用自己存放的路徑
cred = credentials.Certificate('D:/專題/STEM/firebase/serviceAccount.json')

# 初始化firebase，注意不能重複初始化
firebase_admin.initialize_app(cred)

# 初始化firestore
db = firestore.client()
#Function
def ok():
    print(en.get())
    print(en1.get())
    print(en2.get())
    doc = {
    'A': en.get(),
    'B': en1.get(),
    'C': en2.get()
    }

    # 建立文件 必須給定 集合名稱 文件id
    # 即使 集合一開始不存在 都可以直接使用

    # 語法1
    now_time = datetime.datetime.now().strftime('%F %T')
    doc_path = "machine/"+now_time
    # 透過路徑，產生參考
    doc_ref = db.document(doc_path)

    # doc_ref = db.collection("集合名稱").document("文件id")
    # 語法2
    #doc_ref = db.collection("NUUIM").document("student").collection("score").document("score2")

    # doc_ref提供一個set的方法
    doc_ref.set(doc)

#建立主視窗
win = tk.Tk() 
win.title("tkinter 功能連結")
win.geometry("400x200") #寬*高
win.config(bg="#323232")

#標籤
lb = tk.Label(text="This is label")
lb.config(bg="#323232")
lb.config(fg = "white")
lb.pack()

#輸入框
en = tk.Entry()
en.pack()
en1 = tk.Entry()
en1.pack()
en2 = tk.Entry()
en2.pack()

#按鈕
btn = tk.Button(text="確定")
btn.config(command=ok)
btn.pack()

#常駐主視窗
win.mainloop() 